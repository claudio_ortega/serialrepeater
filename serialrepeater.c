/*
 * this program works on an Atmel ATTiny841 with an external Xtal
 *
 * it will replicate any data received on USART1 on pin PA4 into USART0 on pin PA1,
 * but with a different baud rates, as directed from main()
 *
 * it also features an inverter between pins PA0 and PA3,
 * as the signal to be processed comes in inverted
 *
 * the led on pin PB2 will blink at 10Hz during RX/TX activity for about 250 mSec
 */

#include <math.h>
#include <avr/interrupt.h>

// this value should match the one on the X-tal in between PB0/PB1 pins
static const float CLOCK_FREQ_IN_HZ = 11059200.00;
static const long  BLINK_RATE_HZ = 10;

// led & serial activity states
static unsigned char ledIsOn = 0;
static unsigned char serialActivityState = 0;

void signalSerialActivity() 
{
    const float ledHoldOffInMsec = 250;
    serialActivityState = (unsigned char) ( (ledHoldOffInMsec * 0.001) * 2 * BLINK_RATE_HZ );
}

void setLed( int value )
{
    const int LED_PORT_BIT = 1<<PB2;

    DDRB |= LED_PORT_BIT;

    if ( value == 1 )
    {
        PORTB |= LED_PORT_BIT;
    }

    else
    {
        PORTB &= ~LED_PORT_BIT;
    }
}

// interrupt service routine for timer1, 
// it is expected to be executed at a 2*BLINK_RATE_HZ rate
// see function initTimer1Interrupt()
ISR( TIMER1_COMPA_vect )
{
    if ( serialActivityState == 0 )
    {
        setLed(0);
    }
    else
    {
        ledIsOn = !ledIsOn;
        setLed( ledIsOn );
        serialActivityState--;
    }
}

// interrupt service routine for PA0->PA3 level inversion
// it will be executed each time the line PA0 changes level
ISR( PCINT0_vect )
{
    if ( PINA & (1<<PA0) )
    {
        PORTA &= ~(1<<PA3);
    }
    else
    {
        PORTA |= (1<<PA3);
    }
}

void initExternalXtalClock()
{
    // see section 6.6 in the ATTiny841 data sheet
    CCP = 0xD8;    // unlocks next command
    CLKPR = 0x00;  // scale = 1
    
    // wait the safety condition for setting the clock source
    while ( ! ( CLKCR & (1 << OSCRDY) ) ) { }
    
    CCP = 0xD8;    // unlocks next command
    CLKCR = 0x6F;  // use external X-tal oscillator, but do not copy clock signal into pb1
}

void initUSART0Tx( float baudTxBitsSec )  // bits/sec
{
    UCSR0B = (1 << TXEN0);   // enable tx only, see 19.8.2
    UCSR0C = 0b00100110;     // 8 bits, even parity, one stop bit, see 18.12.4

    const unsigned long baudRateCount = lround ( CLOCK_FREQ_IN_HZ / ( 16.00 * baudTxBitsSec ) ) - 1;
    UBRR0H = (baudRateCount >> 8) & 0xff;    
    UBRR0L = baudRateCount & 0xff;    
}

void initUSART1Rx( float baudRxBitsSec )  // bits/sec
{
    UCSR1B = (1 << RXEN1);   // enable rx only, see 19.8.2
    UCSR1C = 0b00100110;     // 8 bits, even parity, one stop bit, see 18.12.4

    const unsigned long baudRateCount = lround ( CLOCK_FREQ_IN_HZ / ( 16.00 * baudRxBitsSec ) ) - 1;
    UBRR1H = (baudRateCount >> 8) & 0xff;
    UBRR1L = baudRateCount & 0xff; 
}

// USART0 tx is on pin PA1
void serialSendOneByteIntoUSART0( unsigned char data )
{
    // wait for the tx USART to be ready to transmit
    while (!(UCSR0A & (1 << UDRE0))) { }

    // transmit the next byte through the USART tx register
    UDR0 = data;
}

// USART1 rx is on pin PA4
unsigned char serialReadOneByteFromUSART1( )
{
    // wait for the rx USART to receive the next byte
    while (!(UCSR1A & (1 << RXC1))) { }

    // read the byte from the USART rx register
    return UDR1;
}

void startUSARTsRepeater()
{
    unsigned char data = 0;

    while (1)
    {
        data = serialReadOneByteFromUSART1();
        serialSendOneByteIntoUSART0( data );
        signalSerialActivity();
    }
}

void startUSARTTxDebug()
{
    unsigned char txDebugSequence = 0;
    unsigned char txDebugData[3] = {0, 0x55, 0xff};
    unsigned char sizeOf = sizeof( txDebugData );

    while (1)
    {
        serialSendOneByteIntoUSART0( txDebugData[ txDebugSequence ] );

        if ( ++txDebugSequence == sizeOf )
        {
            txDebugSequence = 0;
        }
    }
}

// credit: inspired on 
// https://www.instructables.com/id/Arduino-Timer-Interrupts
void initTimer1Interrupt( const float timeOutInSecs )
{
    // planning to use a pre-scaler @1024
    const unsigned long finalCount = ( (unsigned long) ( ( CLOCK_FREQ_IN_HZ / 1024.0 ) * timeOutInSecs ) ) - 1;
    
    cli();    // disable interrupts globally
    
    TCCR1A = 0; // set entire TCCR1A register to 0
    TCCR1B = 0; // same for TCCR1B
    TCNT1  = 0; // initialize counter value to 0
    
    OCR1A = finalCount;
            
    // turn on CTC mode
    TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for a 1024 pre-scaler
    TCCR1B |= (1 << CS12) | (1 << CS10);
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);

    sei();     // re-enable interrupts globally
}

void initInverterInterrupt()
{
    cli();    // disable interrupts globally

    DDRA &= ~(1<<PA0);      // PA0 is the input
    DDRA |= (1<<PA3);       // PA3 is the output

    GIMSK |= (1 << PCIE0);  // allow pin change interrupts. Page 52
    PCMSK0 |= (1<<PCINT0);  // set pin PA0 change interrupt enable
    MCUCR |= (1<<ISC01);    // interruption happens on level change

    sei();     // re-enable interrupts globally
}

void squareSignalOnPB2()
{
    DDRB |= (1<<PB2);

    while (1)
    {
        PORTB ^= (1 << PB2);
        // about 633 nSec/count
        for (unsigned long i = 0; i < 1000; i++) {}
    }
}

int main(void)
{
    initExternalXtalClock();
    initTimer1Interrupt( 1.0 / ( 2.0* (float) BLINK_RATE_HZ ) );
    initInverterInterrupt();
    initUSART0Tx( 115200.0 );
    initUSART1Rx( 100000.0 );
    startUSARTsRepeater();
    return 0;
}
