## Serial Repeater - serial line baud rate cheap conversion


### Motivation ###

I need to read some data coming out from a FrSky X8R remote control receiver.
Such receiver uses a serial protocol, 
sending frames of 25 bytes each, spanning 3 msec, with a 6 msec gap in between.
The baud rate is unfortunately non-standard 100 kbits/sec.
 
The problem is that the final destination for this data is a drone controller 
running on a Rpi using standard unix serial line drivers, 
which do not like baud rates outside the standard set of {300,1200,9600 ... 115200}.

In addition, the line comes out from the X8R with their levels inverted, 
which means that the idle level is low, against the standard.

We solve this with a ATtiny841 micro controller, which features two USARTs.
The program reads from one USART and writes into the second one,
one byte at a time in an infinite loop.
The USART on the RX side is configured at 100k while the one on the TX side is at 115200 bit/sec.
The program uses a couple of interrupt routines to invert the RX pin and to make a led show 
line activity. This last feature is not really needed, but it was done just for fun.
 
### Repository content ###

This repo contains the C source file (1) and MPLAB X-IDE project files (3).
These last were added following advice from this FAQ found in microchip's help site:

    https://microchipdeveloper.com/faq:72

### References ###

(1) device

    https://www.microchip.com/wwwproducts/en/ATtiny841
    http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8495-8-bit-AVR-Microcontrollers-ATtiny441-ATtiny841_Datasheet.pdf

(2) in circuit emulator/debugger

    http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-ICE_UserGuide.pdf

(3) break-out board

    main:
    http://drazzy.com/e/tiny841.shtml
    
    specs:
    https://www.tindie.com/products/drazzy/attiny84184-breakout-wserial-header-bare-board/#specs

    photo:
    http://drazzy.com/e/products/breakout.php#AZB-4

    schematic:
    https://www.tindie.com/products/drazzy/attiny84184-breakout-wserial-header-bare-board/#  (is inside that page, no direct link)
    OR
    ./notes/atttiny841-breakout-schematic.png
    
(4) Ice debugger connector
    
    
    Used the provided 2x3 connector (100 mils).
    See user manual page X, 4.11 "Table 4-11. Atmel-ICE SPI Pin Mapping" and the breakout board schematic, 
    on connector named "ISP".
        

(5) additional software/hardware tools

5.1 - MPLAB X-IDE/IPE, search for those inside microchip website

5.2 - Logic Analyzer - SparkFun/Sigrok/PulseView
    
    SparkFun - PID 15033 USB Logic Analyzer - 25MHz/8-Channel Sigrok
    https://www.sparkfun.com/products/15033
    https://learn.sparkfun.com/tutorials/using-the-usb-logic-analyzer-with-sigrok-pulseview#exploring-the-capabilities
    

(6) used some technical support from

    https://microchipsupport.force.com/

(7) Xc8 compiler 

    microchip.com/MPLABxc
    
(8) device board bill of materials
 
    1x Attiny841 
    1x 1k resistor
    1x led
    1x 11.0592 MHz 50ppm Xtal
    1x 5v-3.3v voltage regulator
    
    Optional: 1x break-out board, see (3) above    

(9) examples 

    https://start.atmel.com/


(10) IPS connector in breakout board

 
 Connnector  Function       Chip
     1         MISO      PA5 (pin 8)     
     2         VCC       VCC (pin 1)
     3         SCK       PA4 (pin 9)
     4         MOSI      PA6 (pin 7)
     5         -RST      PB3 (pin 4)
     6         GND       GND (pin 14)
        
    View of male connector from the component side:  
        
     ---- 
    |1  2|         
    |3  4|
    |5  6|
     ---- 


(11) External connections 

    PA0   serial input 100 kb/sec E,8,1      (pin 13)    10k pull down resistor to GND
    PA1   serial output 115.2 kb/sec E,8,1   (pin 12)
    PA3   PA0 inverted  - short with PA4     (pin 10)
    PA4   PA0 inverted  - short with PA3     (pin 9)
    PB2   output - LED showing activity      (pin 5)
    VCC   5v                                 (pin 1)
    GND   0v                                 (pin 14)
    